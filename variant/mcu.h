
#ifndef VA_INCLUDE_NPORT_VARIANT_MCU_H_
#define	VA_INCLUDE_NPORT_VARIANT_MCU_H_

#include <xc.h>
#include "neon8__manifest.h"

typedef uint8_t nport_mcu__isr__context_t;

#if (NEON8__MANIFEST__ISR__PRIO == 0)
#define NPORT_MCU__ISR__ENABLE()                                            \
    do {                                                                    \
        INTCONbits.GIE = 1;                                                 \
    } while (0)
#elif (NEON8__MANIFEST__ISR__PRIO == 1)
#define NPORT_MCU__ISR__ENABLE()                                            \
    do {                                                                    \
        INTCONbits.GIEL = 1;                                                \
    } while (0)
#elif (NEON8__MANIFEST__ISR__PRIO == 2)
#define NPORT_MCU__ISR__ENABLE()                                            \
    do {                                                                    \
        INTCONbits.GIEH = 1;                                                \
        INTCONbits.GIEL = 1;                                                \
    } while (0)
#else
#error "Unsupported value for NEON8__MANIFEST__ISR__PRIO."
#endif

#if (NEON8__MANIFEST__ISR__PRIO == 0)
#define NPORT_MCU__ISR__DISABLE()                                           \
    do {                                                                    \
        INTCONbits.GIE = 0;                                                 \
    } while (0)
#elif (NEON8__MANIFEST__ISR__PRIO == 1)
#define NPORT_MCU__ISR__DISABLE()                                           \
    do {                                                                    \
        INTCONbits.GIEL = 0;                                                \
    } while (0)
#elif (NEON8__MANIFEST__ISR__PRIO == 2)
#define NPORT_MCU__ISR__DISABLE()                                           \
    do {                                                                    \
        INTCONbits.GIEH = 0;                                                \
        INTCONbits.GIEL = 0;                                                \
    } while (0)
#endif

#define NPORT_MCU__ISR__GET_STATE(state)                                    \
    do {                                                                    \
        *(state) = INTCON;                                                  \
    } while (0)

#define NPORT_MCU__ISR__SET_STATE(state)                                    \
    do {                                                                    \
        INTCON &= ~(_INTCON_GIEH_MASK | _INTCON_GIEL_MASK);                 \
        INTCON |= *(state) & (_INTCON_GIEH_MASK | _INTCON_GIEL_MASK);       \
    } while (0)

#define NPORT_MCU__ISR__SAVE_DISABLE(state)                                 \
    do {                                                                    \
        NPORT_MCU__ISR__GET_STATE(state);                                   \
        NPORT_MCU__ISR__DISABLE();                                          \
    } while (0)

#define NPORT_MCU__ISR__RESTORE(state)                                      \
    NPORT_MCU__ISR__SET_STATE(state)

#endif	/* VA_INCLUDE_NPORT_VARIANT_MCU_H_ */

